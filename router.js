const mongoose = require("mongoose");
const express = require("express");
const bcrypt = require("bcrypt")

const router = express.Router();
const cookieParser = require('cookie-parser')
var db = mongoose.connection;
const jwt = require("jsonwebtoken")
// const productRouter = require("../routes/products");
// const adminProductRouter = require("../routes/admin_products");
const path = require("path");
router.use(express.static(path.join(__dirname, 'frontend')));
router.use(cookieParser())
var UserSchema = new  mongoose.Schema({
    password: String,
    username: String,
    name: String,
    email: String,
    last_name: String,
    image_url: String,
    account_type: String
});


var PostsSchema = new  mongoose.Schema({
    category: String,
    creator: String,
    text_content: String,
    image: String,
    likes_array: Array,
    likes_count: Number,
    date: Date
});


var DocumentsSchema = new  mongoose.Schema({
    category: String,
    creator: String,
    name: String,
    doc: String,
    date: Date
});

var Posts = mongoose.model('Posts', PostsSchema);
var Users = mongoose.model('Users', UserSchema);
var Docs = mongoose.model('Docs', DocumentsSchema);

router.get(["/", "/home"], (req, res) => {
  res.sendFile(path.join(__dirname, "./frontend/log-in.html"));
});

router.get(["/register"], (req, res) => {
  res.sendFile(path.join(__dirname, "./frontend/register.html"));
});
  
router.get(["/profile"], verifyToken, (req, res) => {
  res.sendFile(path.join(__dirname, "./frontend/profile.html"));
});

router.get(["/posts"], verifyToken, (req, res) => {
    res.sendFile(path.join(__dirname, "./frontend/posts-index.html"));
});

router.get(["/documents"], verifyToken, (req, res) => {

    res.sendFile(path.join(__dirname, "./frontend/documents.html"));
});

router.get("/users", async (req, res) => {


    var Users = mongoose.model('Users', UserSchema);
    Users.find({}, function(err, users) {
        var userMap = [];
    
        users.forEach(function(user) {
          userMap.push(user)
        });
        res.send(userMap);  
    })
});


router.get("/posts-list", async (req, res) => {

    Posts.find({}, function(err, posts) {
        var postsMap = [];
    
        posts.forEach(function(posts) {
            postsMap.push(posts)
        });
        postsMap.sort(function(a,b){
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return new Date(b.date) - new Date(a.date);
        });
        res.send(postsMap);  
    })
});

router.post("/users", async (req, res) => {
    Users.find({'email': req.body.email }, function(err, users) {
        var usersMap = [];
        let salt = bcrypt.genSaltSync(10);
        let hashPassword = bcrypt.hashSync(req.body.password, salt);
        req.body.password = hashPassword;
        users.forEach(function(users) {
            usersMap.push(users)
        });

        if(usersMap.length == 0){
            Users.create(req.body);
            res.send(req.body).status("201");
        }else{
            res.send("User already exists").status("203");  
        } 
    })
});


router.put("/users/:id", async (req, res) => {
    if( Object.keys(req.body).includes('password')){
        let salt = bcrypt.genSaltSync(10);
        let hashPassword = bcrypt.hashSync(req.body.password, salt);
        req.body.password = hashPassword;
    }

    Users.findOneAndUpdate({ '_id': req.params.id }, {$set:req.body}, function(err, doc) {
        if (err) return res.send(500, {error: err});
        return res.send(doc);
    });

});


router.get("/username/:username", async (req, res) => {
    Users.find({'username': req.params.username }, function(err, user) {
        if(user.length > 0){
            res.send(false).status("200");
        }else{
            res.send(true).status("200");  
        } 
    })
});




router.get(`/users/:email`, async (req, res) => {

    Users.findOne({'email': req.params.email }, function(err, user) {
        if(user){
            let data = {Email: user.email};
            const token = jwt.sign(data,"secretkey");
            res.cookie('authcookie', token, {httpOnly:true}).json(user).status("200");
        }else{
            res.send(false).status("200");  
        } 
    });
});

router.get(`/user/:id`, async (req, res) => {
    Users.findOne({ '_id': req.params.id }, function(err, doc) {
        if (err) return res.send(500, {error: err});
        return res.send(doc);
    });
    

});


router.post("/posts", async (req, res) => {

    Posts.create(req.body);
    res.send(req.body).status("201")

});


router.delete("/posts/:id", async (req, res) => {
    Posts.findOneAndDelete({ '_id': req.params.id }, function(err, doc) {
        if (err) return res.send(500, {error: err});
        return res.status('204');
    });

})


router.put("/posts/:post_id/:user_id", (req, res) => {
    let post_id = req.params.post_id
    let user_id = req.params.user_id
    Posts.find({}, async function(err, posts) {
        var postsMap = [];
    
        posts.forEach(function(posts) {
            postsMap.push(posts)
        });

        let newPost = postsMap.filter(function(post){
            return post._id == post_id
        });

        let post = newPost[0];  
        if(post.likes_array.includes(user_id)){
            var newArray = post.likes_array.filter(function(f) { return f !== user_id})
            let updatedPost = {
                _id: post._id,
                category: post.category,
                creator: post.creator,
                text_content: post.text_content,
                image: post.image,
                likes_array: newArray,
                likes_count: post.likes_count - 1,
                date: post.date
            }
            Posts.findOneAndUpdate({ '_id': post_id }, updatedPost, function(err, doc) {
                if (err) return res.send(500, {error: err});
                return res.send('Succesfully saved.');
            });
 
        }else{
            let newArray = post.likes_array.concat([user_id]);
            let updatedPost = {
                _id: post._id,
                category: post.category,
                creator: post.creator,
                text_content: post.text_content,
                image: post.image,
                likes_array: newArray,
                likes_count: post.likes_count + 1,
                date: post.date
            }
            Posts.findOneAndUpdate({ '_id': post_id }, updatedPost, function(err, doc) {
                if (err) return res.send(500, {error: err});
                return res.send('Succesfully saved.');
            });
        } 

    })

})


router.get(`/docs/:id`, async (req, res) => {
    Docs.findOne({ '_id': req.params.id }, function(err, doc) {
        if (err) return res.send(500, {error: err});
        return res.send(doc);
    });
});


router.post("/docs", async (req, res) => {

    Docs.create(req.body);
    res.send(req.body).status("201")

})

router.get("/docs", async (req, res) => {

    Docs.find({}, function(err, docs) {
        var docsMap = [];
    
        docs.forEach(function(document) {
            docsMap.push(document)
        });
        docsMap.sort(function(a,b){
            // Turn your strings into dates, and then subtract them
            // to get a value that is either negative, positive, or zero.
            return new Date(b.date) - new Date(a.date);
        });
        res.send(docsMap);  
    })

})


router.get('/delete-cookie', (req, res) => {res.clearCookie('authcookie').send();});

router.post('/pass/:user_id', (req, res) => {
    Users.findOne({ '_id': req.params.user_id }, async function(err, user) {
        if (err) return res.send(500, {error: err});
        let resp = await bcrypt.compare(req.body.password, user.password)
        res.send(resp).status(200)
    })
})


function verifyToken(req, res, next) {

    let token = req.cookies.authcookie;
    
    jwt.verify(token, "secretkey", (err) => {
    
    if(err){
    
        return res.redirect("/home");
    
    }
    
    next();
    
    });
    
};

module.exports = router;