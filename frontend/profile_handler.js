"use strict;"

async function save_user(value){
    let sessionUser = readUserSession()
    let sessionUserId = sessionUser._id  
    await axios.put(`/users/${sessionUserId}`, value).then(res => 
        axios.get(`/user/${sessionUserId}`).then(res2 => {
        writeuserSession(res2.data)
        loadProfile()
        })).catch(err => console.log(err)) //cambiar el user_id por el de la cookie

}

async function check_username(username) {

    if(username.length > 2){
        await axios.get(`username/@${username}`).then(res => {
            if(res.data == true){   //si el nombre de usuario si estsa disponible
                document.getElementById("valid_username").textContent = ""
                document.getElementById('submit').disabled = false;
                check_pass()
            }else{
                document.getElementById("valid_username").textContent = "Username already in use"
                document.getElementById('submit').disabled = true;
            }
        })
    }else{
        document.getElementById("valid_username").textContent = "Username must be at least 3 chars long"
    }

    
}


function loadProfile(){
    const container = document.getElementById("profile-render")
    let sessionUser = readUserSession()
    container.innerHTML = ''
    container.innerHTML += (`
    <div class=" d-flex align-self-center h-50 align-items-center profile-container container">
        <div >
            <div class="portrait">
                <img class="avatar" src="${sessionUser.image_url}">
            </div>
            <div class="form-group" onclick="logout()">
                <input type="submit" value="Logout" class="btn login_btn ml-5" > 
            </div>
        </div>
        <div class="blocks" id="block2"> 
            <div class="links"> 
                <a data-target="#username" data-toggle="modal" href="" >Edit</a> &emsp; Username: ${sessionUser.username}
            </div>
            <div class="links">
                <a data-target="#name" data-toggle="modal" href="" >Edit</a> &emsp; Name: ${sessionUser.name}
            </div>
            <div class="links">
                <a data-target="#lastname" data-toggle="modal" href="" >Edit</a> &emsp; Last Name: ${sessionUser.last_name}
            </div>
            <div class="links">
               Email: ${sessionUser.email}
            </div>
            <div class="links">
                <a data-target="#password_modal" data-toggle="modal" href="" >Edit</a> &emsp; Password: ***********
            </div>
            <div class="links">
                Tipo de Cuenta: ${sessionUser.account_type}
            </div>
        </div>
    </div>
    `)
}

loadProfile()