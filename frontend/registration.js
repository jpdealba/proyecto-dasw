"use strict;"



function check_pass() {
        if (document.getElementById('password').value == document.getElementById('confirm_password').value) {
            console.log(document.getElementById('password').value)
            document.getElementById("valid_text_password").textContent = ""
            document.getElementById('submit').disabled = false;
        } else {
            document.getElementById("valid_text_password").textContent = "Password Must be Matching."
            document.getElementById('submit').disabled = true;
        }
}

async function register_user() {
    let account_type = document.getElementById("exampleFormControlSelect1").value;
    let name = document.getElementById("name").value;
    let last_name = document.getElementById("last_name").value;
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let username = "@" + document.getElementById("new_username").value;
    let image_url = "https://mpng.subpng.com/20180411/rzw/kisspng-user-profile-computer-icons-user-interface-mystique-5aceb0245aa097.2885333015234949483712.jpg"

    await axios.post("/users", {
        account_type,
        name,
        last_name,
        email,
        password,
        username,
        image_url
    }).then(res => {
        console.log(res)
        if(res.status != "200"){
            document.getElementById("not_valid").removeAttribute("hidden")
            throw Error("usuario ya existe")
        }
    })

}

async function login_user() {
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;

    await axios.get(`/users/${email}`).then(async res => {
        let user = res.data
        let resp = await axios.post(`/pass/${user._id}`, {password: password})
        let value = await resp.data
        if(value){
            // bcrypt
            writeuserSession(user)
        }else{
            document.getElementById("not_valid").removeAttribute("hidden")
            throw Error("usuario no existe")
        }
    })
}