"use strict;"

const reader = new FileReader();

const fileUploader = document.getElementById('img');



fileUploader.addEventListener('change', (event) => {
    const files = event.target.files;
    const file = files[0];
    if(imageGrid.hasChildNodes()){
        document.getElementById("imageGrid").innerHTML = ""
    }
    reader.readAsDataURL(file);
    reader.addEventListener('load', (ev) => {
        if(!imageGrid.hasChildNodes()){
            const img = document.createElement('img');
            img.setAttribute("id", "image-upload");
            imageGrid.appendChild(img);
            img.src = ev.target.result;
            img.alt = file.name;
        }
    });

});


async function publish_post() {
    let category = document.getElementById('category').value
    let text_content =  document.getElementById('text').value
    let image = (document.getElementById('image-upload') != undefined) ? document.getElementById('image-upload').src : null
    let sessionUser = readUserSession()
    let creator = sessionUser._id   
    let likes_array = []
    let likes_count = 0;
    let date = new Date()

    await axios.post('/posts', {
        category,
        creator,
        text_content,
        image,
        likes_array,
        likes_count,
        date
    })
}

async function return_posts(){
    let response = await axios.get("/posts-list");
    if(response.status != 200) return []
    let posts = await response.data
    return posts;
}

return_posts().then(res => {
        res.map((product) => {postToHTML(product)})
    },
)

function postToHTML(post) {
    const container = document.getElementById("main-container")
    let sessionUser = readUserSession()
    let sessionUserId = sessionUser._id   
    get_user(post.creator).then(user => {
        let className = post.likes_array.includes(user._id) ? "fas fa-thumbs-up" : "far fa-thumbs-up";
        container.innerHTML += (
            `
            <div class="post-container container" id="${post._id}||${sessionUserId}||${post.likes_array}">
                <div class="row mt-2 ml-2 mb-2" style="">
                        <img class="avatar-mini" style="margin-top: 3%;" src=${user.image_url} alt=""> 
                        <div class="title ml-2" style="margin-top: 5%;">
                            ${user.name}<a href="" >${user.username}</a> &#183; ${moment(post.date).format('DD-MMM-YYYY')} &#183; ${post.category}
                        </div> 
                        
                        <div type="submit" style="font-size: 20px; color: #ffa012; margin-top: 4%; margin-left:auto; margin-right: 2%;" id="like">
                                <i class="${className}" aria-hidden="true" id="thumbs ${post._id}"></i>
                            <span id="objectQuantity ${post._id}">${post.likes_count}</span>
                        </div>   
                        <div id="${post._id}" >
                        "${sessionUserId == post.creator ? '<button type="button" class="btn btn-danger mt-4 mr-2" style="height:50px" >Delete</button>' : ''}"
                        </div>   
                    </div> 
                <p style="color:white;">
                    ${post.text_content}
                </p>
                <div class="d-flex justify-content-center">
                    <img src=${post.image} alt="">
                </div>
            </div>
            `
        )

    })
}

function delete_post(id){
    axios.delete(`/posts/${id}`).then(re => {
        return_posts().then(res => {
            res.map((product) => {postToHTML(product)})
        },
    )
    })
}



const wrapper = document.getElementById('main-container');

wrapper.addEventListener('click', (event) => {
    const isButton = event.target.parentElement.id === 'like';
    if (!isButton) {
      return;
    }
    let id = event.target.offsetParent.id
    let post_id = id.split('||')[0]
    let user_id = id.split('||')[1]
    let likes_array = id.split('||')[2].split(',')
    changeLike(user_id, post_id, likes_array)
  })

var filter = document.getElementById("filter");

filter.addEventListener("change", function() {
    const container = document.getElementById("main-container")
    container.innerHTML = ''
    if(filter.value == "date"){
        console.log(filter.value)
        return_posts().then(res => {
            res.map((product) => {postToHTML(product)})
        })
    }else if(filter.value == "science"){
        return_posts().then(res => {
            let newMap = res.filter(function(post){
                return post.category == "Science"
            })
            newMap.map((product) => {postToHTML(product)})
        })
    }else if(filter.value == "humanities"){
        return_posts().then(res => {
            let newMap = res.filter(function(post){
                console.log(post)
                return post.category == "Humanities"
            })

            newMap.map((product) => {postToHTML(product)})
        })
    }else if(filter.value == "languages"){
        return_posts().then(res => {
            let newMap = res.filter(function(post){
                return post.category == "Languages"
            })
            newMap.map((product) => {postToHTML(product)})
        })
    }
});

async function changeLike(user_id, post_id, likes_array){
    
    await axios.put(`/posts/${post_id}/${user_id}`).then(res => console.log(res)).catch(err => console.log(err))
    if(document.getElementById(`thumbs ${post_id}`).className == "far fa-thumbs-up"){  //si el usuario no esta en la lista lo agrega y cambia a lleno
        let counter =  document.getElementById(`objectQuantity ${post_id}`);
        document.getElementById(`objectQuantity ${post_id}`).innerText = parseInt(counter.innerText) + 1;
        let thumbs = document.getElementById(`thumbs ${post_id}`);
        thumbs.className = "fas fa-thumbs-up"   //este es con relleno
    }
    else if(likes_array.includes(user_id) || document.getElementById(`thumbs ${post_id}`).className == "fas fa-thumbs-up"){  //si el usuario si esta en la lista lo resta y cambia a vacio
       let counter =  document.getElementById(`objectQuantity ${post_id}`);
       document.getElementById(`objectQuantity ${post_id}`).innerText = parseInt(counter.innerText) - 1;
       let thumbs = document.getElementById(`thumbs ${post_id}`);
       thumbs.className = "far fa-thumbs-up"    //este es sin relleno

    }


}


async function get_user(id) {
    let response =  await axios.get(`/user/${id}`);
    let user =  await response.data
    return user
}