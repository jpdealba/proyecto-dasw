function readUserSession(){
    let user = JSON.parse(sessionStorage.getItem('user'))
    return user
}

function writeuserSession(user){
    sessionStorage.setItem('user', JSON.stringify(user))
}

function logout(){
    sessionStorage.clear()
    window.location.href = '/home';
}
async function logout(){
    await axios.get("/delete-cookie").then(() => {
        sessionStorage.clear()
        window.location.href = '/home';
    });
}