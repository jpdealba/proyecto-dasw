"use strict;"
const fileUploader = document.getElementById('doc');
const reader = new FileReader();
fileUploader.addEventListener('change', (event) => {
    const files = event.target.files;
    const file = files[0];
    document.getElementById("imageGrid").innerHTML = ""
    reader.readAsDataURL(file);
    reader.addEventListener('load', (ev) => {
        document.getElementById("imageGrid").innerText = file.name
        document.getElementById("imageGrid").setAttribute("src", ev.target.result)
    
    });

});

async function publish_doc() {
    let category = document.getElementById('category').value
    let doc = document.getElementById('imageGrid').getAttribute("src")
    let sessionUser = readUserSession()
    let creator = sessionUser._id
    let date = new Date()
    let name = document.getElementById('imageGrid').innerText

    await axios.post('/docs', {
        category,
        creator,
        doc,
        name,
        date
    })
}


async function return_docs(){
    let response = await axios.get("/docs");
    if(response.status != 200) return []
    let docs = await response.data
    return docs;
}

return_docs().then(res => {
        res.map((product) => {docToHTML(product)})
    },
)

function docToHTML(doc) {
    const container = document.getElementById("main-container")
    let sessionUser = readUserSession()
    get_user(doc.creator).then(user => {
        container.innerHTML += (
            `
            <div class="post-container container">
                <div class="row mt-2 ml-2 mb-2" style="">
                    <img class="avatar-mini" style="margin-top: 3%;" src=${user.image_url} alt=""> 
                    <div class="title ml-2" style="margin-top: 5%;">
                        ${user.name}<a href="" >${user.username}</a> &#183; ${moment(doc.date).format('DD-MMM-YYYY')} &#183; ${doc.category}
                    </div>  
                </div> 
                <div class="d-flex justify-content-center" style="margin-bottom: 4%;">
                    <a style="color: #ffa012; font-size: large;" id="imageGrid" href=${doc.doc} download="${doc.name}" id="${doc._id}" >
                    ${doc.name}
                    </a>
                </div>
            </div>
            `
        )

    })
}




async function get_user(id) {
    let response =  await axios.get(`/user/${id}`);
    let user =  await response.data
    return user
}
